// Architecture: According to the directive name "datePicker", a consumer will expect to get a completely reusable datapicker.
// However, this datepicker is tightly coupled with To Do application and is not consumable outside.
// From my perspective there are 2 options here:
// 1. Implement a generic "calendar", which just receives the array of dates
// 2. Implement a generic "calendar" and "toDoCalendar" on the top of it to add not reusable behaviour
// 3. Rename it to "toDoCalendar" to keep things simple (worse)
(function(){
    'use strict';
    angular.module('todoListApp').directive('datePicker', function(){

        // Architecture: Parsing should be moved to an upper level (e.g. where a to do is saved).
        // The responsibility of a date picker is to select/highlight date, but not to parse it.
        var dateParser = function(todoList){ // date parser should be in utils, ideally used via "import {dateParser}...
            var dates = []
            var months = {
                Jan: 0, Feb: 1, Mar: 2, Apr: 3, May: 4, Jun: 5, Jul: 6,
                Aug: 7, Sep: 8, Oct: 9, Nov: 10, Dec: 11
            }

            todoList.forEach(function(e){
                // Code: Can we use moment.js here to avoid extra complexity (it's included as a dependency)?
                var date = e.date.split("-")
                dates.push(new Date(date[2], months[date[1]], date[0]))
            })

            return dates
        }

        return function(scope, element, attrs){
            var selectedDates = scope.todoList ? dateParser(scope.todoList) : ""

            $(element).multiDatesPicker({
                onSelect: function(dateText) {

                    scope.todoDate = dateText
                    scope.$apply();
                },
                maxPicks: attrs.picks,
                addDates: selectedDates,
                dateFormat: 'dd-MM-yy'
            })
        }
    });

})();