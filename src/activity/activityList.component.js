(function(){
    'use strict';
    angular.module('todoListApp').component('activityList', {
        templateUrl: function ($element, $attrs) {

            return "./src/activity/activityListTemplate.html"
        }
    });

})();