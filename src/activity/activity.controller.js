(function(){
    'use strict';
    angular.module('todoListApp').controller('activityCtrl', function(activityListLog) {
        var vm = this;

        vm.activityList = activityListLog.getLogsList()
    });

})();