(function(){
    'use strict';
    angular.module('todoListApp').controller('calendarCtrl', function($scope, todoListStore) {
        var vm = this;

        $scope.todoDate = ""

        //TODO: check $scope or vm and impact in todoListCtrl
        $scope.todoList = todoListStore.getTodoList()
    });

})();