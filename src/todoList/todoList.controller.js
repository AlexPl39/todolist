// Formatting: Formatting suffers a little bit in some place.
// Consider to use ESLint (http://eslint.org/) to preserve consisiency and learn new best practises.
(function(){
    'use strict';
    angular.module('todoListApp').controller('todoListCtrl', function($scope, todoListStore, activityListLog) {

        var vm = this
        var actions = activityListLog.getActions()

        // Code: Can we use single property "activeToDo" instead?
        vm.todoTask = ""
        $scope.todoDate = ""
        vm.isEditing = ""
        vm.editingIndex = ""

        // TODO: mover todoList to shared service
        vm.todoList = todoListStore.getTodoList()

        vm.addTask = function(){
            //TODO: add error handling when no date or value is added
            todoListStore.addTodoList($scope.todoDate, vm.todoTask, false)
            activityListLog.addLog($scope.todoDate, vm.todoTask, actions.added)
            vm.todoTask = ""
            $scope.todoDate = ""
        }

        vm.submitTaskChanges = function () {
            todoListStore.editTodoList($scope.todoDate, vm.todoTask)
            activityListLog.addLog($scope.todoDate, vm.todoTask, actions.edit)
            vm.todoTask = ""
            vm.isEditing = false
            $scope.todoDate = ""
        }

        vm.removeItem = function(index){
            activityListLog.addLog($scope.todoDate, vm.todoList[index].task, actions.deleted)
            todoListStore.removeTodoItem(index)
        }

        vm.editItem = function(index){
            vm.isEditing = true

            // TODO: index should be tracked in this controller or in the factory?
            todoListStore.setEditState(index, true)

            vm.todoTask = vm.todoList[index].task
            $scope.todoDate = vm.todoList[index].date
        }
        
        vm.markCompleted = function (index, completed){
            todoListStore.markAsComplete(index)
            var taskDate = vm.todoList[index].date
            var task = vm.todoList[index].task

            if(completed){
                activityListLog.addLog(taskDate, task, actions.activatedBack)
            }
            else {
                activityListLog.addLog(taskDate, task, actions.completed)
            }
        }
        
        vm.cancelEdit = function(index){
            vm.todoTask = ""
            $scope.todoDate = ""
            todoListStore.setEditState(index, false)
        }
    });

})();