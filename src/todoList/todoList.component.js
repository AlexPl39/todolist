(function(){
    'use strict';
    angular.module('todoListApp').component('todoList', {
        templateUrl: function ($element, $attrs) {

          // Naming: "todoTemplate.html" is a little bit confusing here, since the file contains a table, but not a single to do.
          // Should it be "todoListTemplate.html" instead?
          return "./src/todoList/todoTemplate.html"
        }
    });

})();