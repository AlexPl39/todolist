
(function(){
    'use strict';
    angular.module('todoListApp', ['ui.router'])

        .config(function($stateProvider, $urlRouterProvider) {

            $urlRouterProvider.otherwise("/");

            $stateProvider
                .state('main', {
                    url: "/",
                    templateUrl: "src/main/main.html"
                })
                .state('todoList', {
                    url: "/todoList",
                    templateUrl: "src/todoList/todoList.html"
                })
                .state('calendar', {
                    url: "/calendar",
                    templateUrl: "src/calendar/calendar.html",
                    controller: "calendarCtrl"
                })
                .state('activity', {
                    url: "/activity",
                    templateUrl: "src/activity/activity.html"
                })
        });
})();