(function(){
    'use strict';
    angular.module('todoListApp').factory('activityListLog', function () {
            function actions(){
                this.added = "added"
                this.edit = "edited"
                this.completed = "completed"
                this.activatedBack = "marked back as incomplete"
                this.deleted = "deleted"
            }
            var actions = new actions()
            var editIndex = ""
            var activity = [
                {date: "30-Apr-2017, 1:32:31 pm", taskDate: "15-Apr-2017", task: "A task to do", action: actions.added},
                {date: "04-May-2017, 11:07:31 am", taskDate: "1-Apr-2017", task: "Another task that should be done", action: actions.added},
                {date: "17-May-2017, 2:59:31 am", taskDate: "3-Apr-2017", task: "Reminder to ignore the todo list", action: actions.completed}
            ]

            return {
                getActions: function () {
                    return actions;
                },
                getLogsList: function () {
                    //TODO: create a deep array copy to avoid direct references
                    return activity;
                },
                addLog: function (taskDate, task, action) {
                    activity.push({date: moment().format("DD-MMM-YYYY, h:mm:ss a "),taskDate: taskDate, task: task, action: action});
                }
            };
        }

    )})();