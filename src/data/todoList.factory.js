(function(){
    'use strict';
    angular.module('todoListApp').factory('todoListStore', function () {
        var editIndex = ""
        var todoList = [
            {date: "01-May-2017", task: "A task to do", isEditing: false, completed: false},
            {date: "04-May-2017", task: "Another task that should be done", isEditing: false, completed: false},
            {date: "17-May-2017", task: "Reminder to ignore the todo list", isEditing: false, completed: true}
        ]

        return {
            getTodoList: function () {
                //TODO: create a deep array copy to avoid direct references
                return todoList;
            },
            addTodoList: function (date, task, isEditing) {
                todoList.push({date: date, task: task, isEditing: isEditing});
            },
            setEditState: function (index, isEditing) {
                editIndex = index
                todoList[index].isEditing = isEditing
            },
            markAsComplete: function (index) {
                todoList[index].completed = !todoList[index].completed
                editIndex = ""
            },
            editTodoList: function (date, task) {
                todoList[editIndex].task = task
                todoList[editIndex].date = date
                todoList[editIndex].isEditing = false
                todoList[editIndex].completed = false
                editIndex = ""
            },
            removeTodoItem: function (index) {
                todoList.splice(index, 1)
            }

        };
    }

)})();